import setuptools

setuptools.setup(
    name="hello_world_practice",
    version="1.0.2",
    url = "https://gitlab.com/hello_world_practice_python_package.git",
    author="Jeremy Bingham",
    author_email="jem.luke@gmail.com",
    description="A small practice package containing a function which prints",
    packages=setuptools.find_packages(),
)