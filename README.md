# Practice hello world package for SEAMS 2020!

To install this package, you should use a command line tool and have pip installed.
Navigate to the package directory (containing "setup.py" and a folder called "hello_world_practice").
Run "pip install -e ." (alternatively, you could substitute the path to the folder containing the package in place of ".".


To use the package, in python run "from hello_world_practice import functions".
The package contains one main function "sayHello()", which accepts a single optional string argument.
If an appropriate argument is provided, the function will print the provided argument; otherwise the function prints "Hello World!".


example:

from hello_world_practice import functions
functions.sayHello("Hi!")

>>> Hi!